package com.utp.model;

import android.Manifest;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Environment;
import android.os.FileUtils;
import android.util.Log;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Puzzle {

    private final String PATH_IMAGE = "puzzle_element/";
    private int n;
    private int pos_current;
    private enum OPERATION {UP, DOWN, RIGHT, LEFT};
    private List<PuzzleElement> listPuzzleElement;
    //private int [] posCurrentTemp;

    public Puzzle(int n, int pos_current, List<PuzzleElement> listPuzzleElement) {
        this.n = n;
        this.pos_current = pos_current;
        this.listPuzzleElement = listPuzzleElement;
    }

    public void setImageRandom(){

        List<Integer> listPosCurrent = new ArrayList<>();
        List<Integer> listPosCurrentFinal = new ArrayList<>();

        for(int i=0; i<n*n; i++){
            if(i != 6) {
                listPosCurrent.add(i);
                Log.i("pos_current" + i, String.valueOf(i));
            }
        }
        Collections.shuffle(listPosCurrent);
        for(int i=0; i<(n*n)-1; i++){
            Log.i("pos_current_mod" + i, String.valueOf(listPosCurrent.get(i)));
        }

        int index = 0;
        for(int i=0; i<n*n; i++){
            if(i != 6){
                int value = listPosCurrent.get(index);
                listPosCurrentFinal.add(value);
                index++;
            }else{
                listPosCurrentFinal.add(6);
            }

            Log.i("pos_current_mod_final" + i, String.valueOf(listPosCurrentFinal.get(i)));
        }



        for(int i=0; i<listPuzzleElement.size(); i++){
            int newPos = listPosCurrentFinal.get(i);
            Drawable drawable = listPuzzleElement.get(newPos).getDrawable();
            listPuzzleElement.get(i).getImageButton().setBackground(drawable);

            File path = Environment.getExternalStoragePublicDirectory(
                    Environment.DIRECTORY_PICTURES);
            //listPuzzleElement.get(i).getImageButton().getBackground().get
            //listPuzzleElement.get(i).getImageButton().getp
            Bitmap bitmap = drawableToBitmap(drawable);
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG,100,outputStream);
            byte[] bytes = outputStream.toByteArray();
            //File file = new File(path, "DemoPicture.jpg");
            try {
                File file = new File(path.getPath()+"/imagen-"+i+".jpg");
                if (!file.exists()) {
                    System.out.println(file.getPath());
                    file.createNewFile();
                }
                FileOutputStream fileOutputStream = new FileOutputStream(file);
                fileOutputStream.write(bytes);
                fileOutputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

            //guardar los imagenes en directorio public de images
        }

    }


    /*
    public void setImageRandom(){

        for(int i=0; i<n*n; i++){
            int pos_current = (int)(Math.random()*10);
            if(isUsedPosition(pos_current)){
                pos_current = (int)(Math.random()*10);
            }else{

            }
            Log.i("pos_current" + i, String.valueOf(pos_current));
            //listPuzzleElement.get(i).setPos_current();
            //listPuzzleElement.add(new PuzzleElement("image_part_00"+String.valueOf(i+1)+".jpg", 1, 1));
        }

    }

    public boolean isUsedPosition(int pos_current){
        boolean result = false;
        for(int i=0; i<listPuzzleElement.size(); i++){
            if(listPuzzleElement.get(i).getPos_current() == pos_current){
                result = true;
                break;
            }
        }
        return result;
    }
    */

    public int getN() {
        return n;
    }

    public void setN(int n) {
        this.n = n;
    }

    public int getPos_current() {
        return pos_current;
    }

    public void setPos_current(int pos_current) {
        this.pos_current = pos_current;
    }

    public List<PuzzleElement> getListPuzzleElement() {
        return listPuzzleElement;
    }

    public void setListPuzzleElement(List<PuzzleElement> listPuzzleElement) {
        this.listPuzzleElement = listPuzzleElement;
    }

    private  Bitmap drawableToBitmap (Drawable drawable) {
        Bitmap bitmap = null;

        if (drawable instanceof BitmapDrawable) {
            BitmapDrawable bitmapDrawable = (BitmapDrawable) drawable;
            if(bitmapDrawable.getBitmap() != null) {
                return bitmapDrawable.getBitmap();
            }
        }

        if(drawable.getIntrinsicWidth() <= 0 || drawable.getIntrinsicHeight() <= 0) {
            bitmap = Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888); // Single color bitmap will be created of 1x1 pixel
        } else {
            bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        }

//        Canvas canvas = new Canvas(bitmap);
//        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
//        drawable.draw(canvas);
        return bitmap;
    }



}
