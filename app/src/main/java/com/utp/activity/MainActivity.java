package com.utp.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

import com.utp.model.Puzzle;
import com.utp.model.PuzzleElement;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    ImageButton ibOne;
    ImageButton ibTwo;
    ImageButton ibThree;
    ImageButton ibFour;
    ImageButton ibFive;
    ImageButton ibSix;
    ImageButton ibSeven;
    ImageButton ibEight;
    ImageButton ibNine;

    Button bStart;

    Puzzle puzzle;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Log.i("isExternalStorageW ", String.valueOf(isExternalStorageWritable()));

        //File file = new File(path, "DemoPicture.jpg");

        loadImages();
        loadPuzzle();
        checkPermission();

        bStart = (Button)findViewById(R.id.bStart);
        bStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                puzzle.setImageRandom();
            }
        });
    }

    public void loadPuzzle(){
        int n = 3;
        List<PuzzleElement> listPuzzleElement = new ArrayList<>();
        //for(int i=0; i<n*n; i++){
            //listPuzzleElement.add(new PuzzleElement("image_part_00"+String.valueOf(i+1)+".jpg", 1, 1));
            //listPuzzleElement.add(new PuzzleElement("image_part_00"+String.valueOf(i+1)+".jpg", 1, 1));
        //}

        listPuzzleElement.add(new PuzzleElement(ibOne, 1, 1));
        listPuzzleElement.add(new PuzzleElement(ibTwo, 2, 2));
        listPuzzleElement.add(new PuzzleElement(ibThree, 3, 3));
        listPuzzleElement.add(new PuzzleElement(ibFour, 4, 4));
        listPuzzleElement.add(new PuzzleElement(ibFive, 5, 5));
        listPuzzleElement.add(new PuzzleElement(ibSix, 6, 6));
        listPuzzleElement.add(new PuzzleElement(ibSeven, 7, 7));
        listPuzzleElement.add(new PuzzleElement(ibEight, 8, 8));
        listPuzzleElement.add(new PuzzleElement(ibNine, 9, 9));


        puzzle = new Puzzle(n, 7, listPuzzleElement);
    }

    public void loadImages(){

        ibOne = (ImageButton)findViewById(R.id.ibOne);
        ibTwo = (ImageButton)findViewById(R.id.ibTwo);
        ibThree = (ImageButton)findViewById(R.id.ibThree);
        ibFour = (ImageButton)findViewById(R.id.ibFour);
        ibFive = (ImageButton)findViewById(R.id.ibFive);
        ibSix = (ImageButton)findViewById(R.id.ibSix);
        ibSeven = (ImageButton)findViewById(R.id.ibSeven);
        ibEight = (ImageButton)findViewById(R.id.ibEight);
        ibNine = (ImageButton)findViewById(R.id.ibNine);


        ibOne.setBackgroundResource(R.drawable.image_part_001);
        ibTwo.setBackgroundResource(R.drawable.image_part_002);
        ibThree.setBackgroundResource(R.drawable.image_part_003);
        ibFour.setBackgroundResource(R.drawable.image_part_004);
        ibFive.setBackgroundResource(R.drawable.image_part_005);
        ibSix.setBackgroundResource(R.drawable.image_part_006);
        ibSeven.setBackgroundColor(getResources().getColor(android.R.color.darker_gray));
        ibEight.setBackgroundResource(R.drawable.image_part_008);
        ibNine.setBackgroundResource(R.drawable.image_part_009);

    }

    public boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        }
        return false;
    }
    private void checkPermission(){
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)!= PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            } else {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        1);
            }
        }
    }

}
